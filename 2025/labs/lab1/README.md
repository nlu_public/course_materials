# Instructions

First follow the instructions in NLU+ Coursework 1 and create the `nlu` conda environment. Then activate the environment, and install JupyterLab with pip:

```shell
pip install notebook
```

Once installed, launch Jupyter notebook with:
```shell
jupyter notebook
```
and open the `lab1.ipynb` file.


### Alternative: Lab1-specific environment

As an alternative, you can create and work with a virtual environment just for Lab1.
Simply run:
```shell
conda create -n lab1 python=3.7
```
then, activate the environment and install the required packages:
```shell
conda activate lab1
pip install numpy==1.21.5 scipy==1.7.3 notebook==6.4.8
```
and launch Jupyter notebook with:
```shell
jupyter notebook
```
