# Instructions

A Google Colab version of this notebook is available at this link: https://colab.research.google.com/drive/1ctp6hTz3mIUpnLZm-TL5-hmOIOp-htCg?usp=sharing

### Development Environment

First follow the instructions in NLU+ Coursework 1 to install conda. Then,
create a new (conda) virtual environment for Lab2. Simply run:

```shell
conda create -n lab2 python=3.7
```

then, activate the environment and install the required packages:

```shell
conda activate lab2
pip install numpy==1.21.5 scipy==1.7.3 notebook==6.4.8 nltk==3.6.7 umap-learn==0.5.2 matplotlib==3.5.1 jinja2==3.0.3
python -m nltk.downloader 'punkt'
```

and launch Jupyter notebook with:

```shell
jupyter notebook
```

### Download Data

Go (i.e., `cd`) into the directory of the lab. First, download and extract the
word embeddings you will be using for the lab.

```shell
wget -c https://nlp.stanford.edu/data/glove.6B.zip
unzip glove.6B.zip
```

Download and extract the DBpedia text classification dataset.

```shell
wget -c https://github.com/pyk/dbpedia_csv/raw/master/dbpedia_csv.tar.gz
tar -xvzf dbpedia_csv.tar.gz
```